<?php

/**
 * Plugin Name: API Apprenants
 * Description: Plugin pour créer les champs personnalisés et les type de contenus personnalisés
 * Author: Gabriel Thiebaut
 * Version: 0.0.1
 * Licence: GPL
 */

function wpm_custom_post_type()
{
    // Initialisation de la taxonomie Promotion

    // Labels de la taxonomie Promotion
    $labels_taxo = array(
        // Le nom au pluriel
        'name'                => ('Promotions'),
        // Le nom au singulier
        'singular_name'       => ('Promotion'),
        // Le libellé affiché dans le menu
        'menu_name'           => ('Promotions'),
        // Les différents libellés de l'administration
        'all_items'           => ('Toutes les promotions'),
        'view_item'           => ('Voir les promotions'),
        'add_new_item'        => ('Ajouter une promotion'),
        'add_new'             => ('Ajouter'),
        'edit_item'           => ('Editer une promotion'),
        'update_item'         => ('Modifier une promotion'),
        'search_items'        => ('Rechercher une promotion'),
        'not_found'           => ('Non trouvé'),
        'not_found_in_trash'  => ('Non trouvé dans la corbeille'),
    );


    // $args de la taxonomie Promotion
    $promotaxo = array(
        'description'   => 'Taxonomie',
        'hierarchical'  => false,
        'show_in_rest'  => true,
        'labels'        => $labels_taxo,
    );

    // Enregistrement de la taxonomie Promotion
    register_taxonomy('Promotion', array('apprenants'), $promotaxo);


    // Initialisation de la taxonomie Compétences

    // Labels de la taxonomie Promotion
    $labels_compe = array(
        // Le nom au pluriel
        'name'                => ('Compétences'),
        // Le nom au singulier
        'singular_name'       => ('Compétence'),
        // Le libellé affiché dans le menu
        'menu_name'           => ('Compétences'),
        // Les différents libellés de l'administration
        'all_items'           => ('Toutes les Compétences'),
        'view_item'           => ('Voir les Compétences'),
        'add_new_item'        => ('Ajouter une Compétence'),
        'add_new'             => ('Ajouter'),
        'edit_item'           => ('Editer une Compétence'),
        'update_item'         => ('Modifier une Compétence'),
        'search_items'        => ('Rechercher une Compétence'),
        'not_found'           => ('Non trouvé'),
        'not_found_in_trash'  => ('Non trouvé dans la corbeille'),
    );


    // $args de la taxonomie Promotion
    $competaxo = array(
        'description'   => 'Taxonomie',
        'hierarchical'  => false,
        'show_in_rest'  => true,
        'labels'        => $labels_compe,
    );

    // Enregistrement de la taxonomie Promotion
    register_taxonomy('Competence', array('apprenants'), $competaxo);



    // Initialisation du custom post Apprenant

    //Supports du custom post Apprenant
    $supports = array(
        'title',
        'excerpt',
        'thumbnail',
    );

    // Labels du custom post Apprenant
    $labels_apprenant = array(
        // Le nom au pluriel
        'name'                => ('Apprenants'),
        // Le nom au singulier
        'singular_name'       => ('Apprenant'),
        // Le libellé affiché dans le menu
        'menu_name'           => ('Apprenants'),
        // Les différents libellés de l'administration
        'all_items'           => ('Tous les apprenants'),
        'view_item'           => ('Voir les apprenants'),
        'add_new_item'        => ('Ajouter un apprenant'),
        'add_new'             => ('Ajouter'),
        'edit_item'           => ('Editer un apprenant'),
        'update_item'         => ('Modifier un apprenant'),
        'search_items'        => ('Rechercher un apprenant'),
        'not_found'           => ('Non trouvé'),
        'not_found_in_trash'  => ('Non trouvé dans la corbeille'),
    );

    // $args du custom post Aprennant
    $args = array(
        'label'               => 'Apprenants',
        'labels'              => $labels_apprenant,
        'description'         => 'liste aprenants',
        'menu_icon'           => 'dashicons-admin-users',
        'public'              => true,
        'supports'            => $supports,
        'taxonomies'          => array('Promotion'),
        'show_in_rest'        => true
    );

    // On enregistre notre custom post
    register_post_type('apprenants', $args);
}


// Hook qui lance la fonction
add_action('init', 'wpm_custom_post_type', 0);

function api_edn_expose_more_info($data, $post)
{
    $featured_image_url = get_the_post_thumbnail_url($post) ? get_the_post_thumbnail_url($post, 'original') : "";
    
    $promotion = get_the_terms($post, 'Promotion');
    $promotion_infos = [
        'id'    => $promotion[0]->term_id,
        'name'  => $promotion[0]->name,
        'description'  => $promotion[0]->description,
        'slug'  => $promotion[0]->slug,
    ];

    $competences = get_the_terms($post, 'Competence');        
    $competences_infos[] = array();
    foreach($competences as $key => $competence) {
        $competences_infos[$key]['id'] = $competence->term_id;
        $competences_infos[$key]['name'] = $competence->name;
        $competences_infos[$key]['description'] = $competence->description;
        $competences_infos[$key]['slug'] = $competence->slug;
        
    }
    // print_r($competences_infos);
    $data->data['featured_media'] = $featured_image_url;
    $data->data['promotion'] = $promotion_infos;
    $data->data['competences'] = $competences_infos;

    return $data;
}

add_filter('rest_prepare_apprenants', 'api_edn_expose_more_info', 10, 2);

function filter_apprenantslist_json($data, $post, $context)
{
    $api_name = get_post_meta($post->ID, '_the_name', true);
    $api_surname = get_post_meta($post->ID, '_the_surname', true);
    $portfolio = get_post_meta($post->ID, '_url_port', true);
    $linkedin = get_post_meta($post->ID, '_url_linkedin', true);
    $cv_file = get_post_meta($post->ID, '_cv_file', true);
    $thumb_url= get_the_post_thumbnail_url($post->ID);

    if ($api_name) {
        $data->data['prenom'] = $api_name;
    }

    if ($api_surname) {
        $data->data['nom'] = $api_surname;
    }

    if ($portfolio) {
        $data->data['portfolio'] = $portfolio;
    }

    if ($linkedin) {
        $data->data['linkedin'] = $linkedin;
    }

    if ($cv_file) {
        $data->data['cv'] = $cv_file;
    }

    if ($thumb_url) {
        $data->data['image'] = $thumb_url;
    }

    return $data;
}
add_filter('rest_prepare_apprenants', 'filter_apprenantslist_json', 10, 3);

add_action('add_meta_boxes', 'init_metabox');
function init_metabox()
{
    add_meta_box('info_apprenants', 'Informations', 'info_apprenants', 'apprenants', 'normal');
};

function info_apprenants($post)
{

    $theName = get_post_meta($post->ID, '_the_name', true);
    echo '<div  class="meta-divs" style="margin: 10px 0px; background:whitesmoke; padding:8px; border-radius: 5px;">';
    echo '<label for="text_meta"><p>Prénom</p></label>';
    echo '<input id="text_meta" type="text" name="the_name" value="' . $theName . '" />';
    echo '</div>';

    $theSurName = get_post_meta($post->ID, '_the_surname', true);
    echo '<div class="meta-divs" style="margin: 10px 0px; background:whitesmoke; padding:8px; border-radius: 5px;">';
    echo '<label for="text_meta"><p>Nom</p></label>';
    echo '<input id="text_meta" type="text" name="the_surname" value="' . $theSurName . '" />';
    echo '</div>';

    $urlport = get_post_meta($post->ID, '_url_port', true);
    echo '<div class="meta-divs"  style="margin: 10px 0px; background:whitesmoke; padding:8px; border-radius: 5px;">';
    echo '<label for="url_meta"><p>Portfolio</p></label>';
    echo '<input id="url_meta" type="url" name="url_port" value="' . $urlport . '" />';
    if ($urlport){
        echo '<p>Lien : <a href="' . $urlport . '"> ' . $urlport . ' </a> </p>';
    };
    echo '</div>';

    $urllkin = get_post_meta($post->ID, '_url_linkedin', true);
    echo '<div class="meta-divs"  style="margin: 10px 0px;  background:whitesmoke; padding:8px; border-radius: 5px;">';
    echo '<label for="url_meta"><p>LinkedIn</p></label>';
    echo '<input id="url_meta" type="url" name="url_linkedin" value="' . $urllkin . '" />';
    if ($urllkin){
        echo '<p>Lien : <a href="' . $urllkin . '"> ' . $urllkin . ' </a> </p>';
    };
    echo '</div>';

    $cvfile = get_post_meta($post->ID, '_cv_file', true);
    echo '<div class="meta-divs" style="margin: 10px 0px;  background:whitesmoke; padding:8px; border-radius: 5px;">';
    echo '<label for="url_meta"><p>CV</p></label>';
    echo '<input id="url_meta" type="url" name="cv_file" value="' . $cvfile . '" />';
    if ($cvfile){
        echo '<p>Lien : <a href="' . $cvfile . '"> ' . $cvfile . ' </a> </p>';
    };
    echo '</div>';
};

add_action('save_post', 'save_metabox');
function save_metabox($post_id)
{
    if (isset($_POST['the_name']))
        update_post_meta($post_id, '_the_name', esc_attr($_POST['the_name']));

    if (isset($_POST['the_surname']))
        update_post_meta($post_id, '_the_surname', esc_attr($_POST['the_surname']));

    if (isset($_POST['url_port']))
        update_post_meta($post_id, '_url_port', esc_url($_POST['url_port']));

    if (isset($_POST['url_linkedin']))
        update_post_meta($post_id, '_url_linkedin', esc_url($_POST['url_linkedin']));

    if (isset($_POST['cv_file']))
        update_post_meta($post_id, '_cv_file', esc_url($_POST['cv_file']));
};